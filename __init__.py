# Conway's Game of Life
# by HarveyKG (kramski@web.de - not a Python programmer, not even a daily programmer...)

from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from st3m.utils import tau
from ctx import Context
import st3m.run
import random

# Global Variables

# Screen Size
screenWidth  = 240
screenHeight = 240
cellSize = 24
# cellSize = 16
# cellSize = 12

# Array Size
gens = 2
rows = screenHeight // cellSize + 1
cols = screenWidth  // cellSize + 1
# https://stackoverflow.com/questions/10668341/create-3d-array-using-python
cells = [[[0 for col in range(cols)] for row in range(rows)] for gen in range(gens)]

firstRun = True
currentGen = 0
nextGen    = 1


def clearGeneration(gen):
    for col in range(cols):
        for row in range(rows):
            cells[gen][row][col] = 0

class CGoL(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        # Ignore the app_ctx for now.

        def setInitialCells():
            # TDOD: need an interactive Editor

            # q&d: Glider
            cells[0][7][8] = 1
            cells[0][8][9] = 1
            cells[0][9][7] = cells[0][9][8] = cells[0][9][9] = 1

        clearGeneration(0)
        clearGeneration(1)
        setInitialCells()

    def draw(self, ctx: Context) -> None:
        global firstRun
        global currentGen
        global nextGen

        def drawGrid():
            # Paint the background black
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

            # Draw the Grid
            for x in range(-1 * screenWidth // 2, screenWidth // 2, cellSize):
                for y in range(-1 * screenHeight // 2, screenHeight // 2, cellSize):
                    ctx.rgb(0.5, 0.5, 0.5).rectangle(x, y, cellSize, cellSize).stroke()

        def drawGeneration(gen):
            row = 0
            col = 0
            for x in range(-1 * screenWidth // 2, screenWidth // 2, cellSize):
                for y in range(-1 * screenHeight // 2, screenHeight // 2, cellSize):
                    if cells[gen][row][col]:
                        ctx.rgb(0.0, 1.0, 0.0).arc(x + cellSize // 2, y + cellSize // 2, cellSize // 2 - 2, 0, tau, 0).fill()
                        # ctx.rgb(random.randint(0, 100) / 100, random.randint(0, 100) / 100, random.randint(0, 100) / 100).arc(x + cellSize // 2, y + cellSize // 2, cellSize // 2 - 2, 0, tau, 0).fill()
                    else:
                        ctx.rgb(0.4, 0.4, 0.4).arc(x + cellSize // 2, y + cellSize // 2, cellSize // 2 - 2, 0, tau, 0).fill()
                        # ctx.rgb(random.randint(0, 100) / 100, random.randint(0, 100) / 100, random.randint(0, 100) / 100).arc(x + cellSize // 2, y + cellSize // 2, cellSize // 2 - 2, 0, tau, 0).fill()
                    row += 1
                col += 1
                row = 0


        def neighbours(gen, row, col):
            # das geht bestimmt eleganter

            # return 2
            n = 0

            # Wrap around
            rminus = row - 1
            if rminus < 0:
                rminus = rows - 1
            rplus = row + 1
            if rplus >= rows:
                rplus = 0
            cminus = col - 1
            if cminus < 0:
                cminus = cols - 1
            cplus = col + 1
            if cplus >= cols:
                cplus = 0

            # Count living Neighbours
            if cells[gen][rminus][cminus]:
                n += 1
            if cells[gen][rminus][col]:
                n += 1
            if cells[gen][rminus][cplus]:
                n += 1
            if cells[gen][row][cminus]:
                n += 1
            if cells[gen][row][cplus]:
                n += 1
            if cells[gen][rplus][cminus]:
                n += 1
            if cells[gen][rplus][col]:
                n += 1
            if cells[gen][rplus][cplus]:
                n += 1

            return n


        def newGeneration(cur, nxt):
            clearGeneration(nxt)
            for col in range(cols):
                for row in range(rows):
                    if neighbours(cur, row, col) < 2:
                        # Death
                        cells[nxt][row][col] = 0
                    elif neighbours(cur, row, col) == 2:
                        # Continuation
                        cells[nxt][row][col] = cells[cur][row][col]
                    elif neighbours(cur, row, col) == 3:
                        # Birth
                        cells[nxt][row][col] = 1
                    else:
                        # Death
                        cells[nxt][row][col] = 0


        # Main Loop

        # if firstRun:
        #    drawGrid()
        #    firstRun = False

        drawGrid()

        drawGeneration(currentGen)
        newGeneration(currentGen, nextGen)

        if currentGen == 0:
            currentGen = 1
            nextGen    = 0
        else:
            currentGen = 0
            nextGen    = 1

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing


if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(CGoL(ApplicationContext()))
